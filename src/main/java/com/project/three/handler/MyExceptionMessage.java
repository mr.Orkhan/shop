package com.project.three.handler;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;


public class MyExceptionMessage extends RuntimeException {

    public MyExceptionMessage(String message) {
        super(message);
    }

    public MyExceptionMessage(String message, Throwable cause) {
        super(message, cause);

    }

}
