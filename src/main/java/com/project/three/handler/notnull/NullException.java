package com.project.three.handler.notnull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;


@Getter
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
public class NullException {
    String message;
    Throwable throwable;
    HttpStatus httpStatus;
    ZonedDateTime zoneDateTime;

    public NullException(String message,
                         Throwable throwable,
                         HttpStatus httpStatus,
                         ZonedDateTime zoneDateTime) {
        this.message = message;
        this.throwable = throwable;
        this.httpStatus = httpStatus;
        this.zoneDateTime = zoneDateTime;
    }
}
