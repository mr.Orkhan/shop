package com.project.three.handler.notnull;

import com.project.three.handler.MyExceptionMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
@ControllerAdvice
public class NotNullExceptionHandler {
    @ExceptionHandler(value ={MyExceptionMessage.class} )
    public ResponseEntity<Object> handlerNotNull(MyExceptionMessage nne){
    NullException n= new NullException(
             nne.getMessage(),
             nne,
             HttpStatus.BAD_REQUEST,
             ZonedDateTime.now());
     return new ResponseEntity<>(n,HttpStatus.BAD_REQUEST);
    }
}
