package com.project.three.repository;

import com.project.three.entity.basket.BasketProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HomeCountRepository extends JpaRepository<BasketProduct,Long> {
    List<BasketProduct> findByBasket_id(Long id);
}
