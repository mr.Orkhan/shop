package com.project.three.repository;

import com.project.three.entity.login.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepository extends JpaRepository<Authority,Long> {

}
