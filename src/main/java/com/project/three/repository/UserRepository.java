package com.project.three.repository;

import com.project.three.entity.login.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findByName(String username);

    boolean existsByEmail(String email);

    User findByEmail(String email);

    //User findByBody(Integer str);
}
