package com.project.three.repository;

import com.project.three.entity.home.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {
    Address findByHome_id(Long id);


    //boolean existsByAddress(String address);
}
