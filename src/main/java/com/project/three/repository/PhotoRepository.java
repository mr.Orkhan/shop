package com.project.three.repository;

import com.project.three.entity.photo.PhotoEdit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoRepository extends JpaRepository<PhotoEdit,Long> {
}
