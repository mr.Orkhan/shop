package com.project.three.repository;

import com.project.three.entity.home.HomeFirstPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FirstPhotoRepository extends JpaRepository<HomeFirstPhoto,Long> {

   // @Query("select hr from HomeFirstPhoto hr join fetch hr.homeOtherPhotoList")
    @Override
    List<HomeFirstPhoto> findAll();

    HomeFirstPhoto findByHome_id(Long id);
}
