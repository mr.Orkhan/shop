package com.project.three.repository;

import com.project.three.entity.login.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message,Long> {

    Message findByEmail(String email);

    Message findByBody(Integer str);

    boolean existsByEmail(String email);
}
