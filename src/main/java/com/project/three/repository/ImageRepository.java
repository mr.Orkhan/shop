package com.project.three.repository;

import com.project.three.entity.photo.ImageData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<ImageData,Long> {


    ImageData findByName(String fileName);
}
