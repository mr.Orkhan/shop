package com.project.three.repository;

import com.project.three.entity.jwt.Jwt;
import com.project.three.entity.login.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JwtRepository extends JpaRepository<Jwt,Long> {

    Jwt findByUser_Id(Long id);
}
