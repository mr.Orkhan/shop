package com.project.three.repository;

import com.project.three.entity.home.Home;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Optional;

public interface HomeRepository extends JpaRepository<Home,Long>, JpaSpecificationExecutor<Home> {

    List<Home> findByPriceBetween(Double i,Double a);

    //@Query("select h from Home h join fetch h.homeOtherPhotoList hop")

    Optional<Home> findById(Long id);

    @Override
    Page<Home> findAll(Pageable pageable);

    //Page<Home> findAllByOrderByLocalDateTime(Specification<Home> homeSpecification, Pageable pageable);


    // List<Home> findAllByOrderByLocalDateTime(String name);

}
