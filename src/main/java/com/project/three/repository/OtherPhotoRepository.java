package com.project.three.repository;

import com.project.three.entity.home.HomeOtherPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OtherPhotoRepository extends JpaRepository<HomeOtherPhoto,Long> {

    List<HomeOtherPhoto> findByHome_id(Long id);
}
