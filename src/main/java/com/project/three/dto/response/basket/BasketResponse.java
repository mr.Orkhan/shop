package com.project.three.dto.response.basket;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BasketResponse {
    String hotelName;
    Double price;
    Integer count;
}
