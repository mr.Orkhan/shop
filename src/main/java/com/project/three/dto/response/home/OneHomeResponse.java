package com.project.three.dto.response.home;

import com.project.three.dto.response.comment.CommentResponse;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OneHomeResponse {
    String title;
    Double price;
    String cityName;
    String description;
    String addressName;
    String image;
    List<String> images;
    List<CommentResponse> commentResponses;

}
