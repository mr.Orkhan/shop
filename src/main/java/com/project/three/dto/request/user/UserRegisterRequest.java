package com.project.three.dto.request.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRegisterRequest {
    @NotNull(message = "should write name")
    String name;
    @NotNull(message = "should write surname")
    String surname;
    @JsonFormat(pattern="yyyy-MM-dd")
    String birthDay;
    @NotNull(message = "should write username")
    String username;
    @NotNull(message = "should write password")
    String password;

    String email;
}
