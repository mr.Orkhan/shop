package com.project.three.dto.request.home;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
//@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class HomeRequest {
    @NotNull(message = "title-should not be null")
    String title;
    @NotNull(message = "description-should not be null")
    String description;
    Double price;
    @NotNull(message = "cityName-should not be null")
    String cityName;
   // @NotNull(message = "addressName-should not be null")
     String addressName;
    @Column(length = 100000)
    @NotNull(message = "image-should not be null")
    String image;
    Long userId;

    @Length(max = 5)
    List<String> images;
}
