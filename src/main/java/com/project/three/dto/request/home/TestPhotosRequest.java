package com.project.three.dto.request.home;


import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TestPhotosRequest {
    Long userId;

    String[] images=new String[5];
}
