package com.project.three.dto.request.comment;


import com.project.three.entity.home.Home;
import com.project.three.entity.login.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CommentRequest {

    String comment;
    Long homeId;
    Long userId;
}
