package com.project.three.dto.request.home;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class HomeFirstPhotoRequest {

    //@Column(length = 100000)

    String imgPath;

}
