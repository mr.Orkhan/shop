package com.project.three.filter;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@FieldDefaults(level = AccessLevel.PRIVATE)

public class HomeFilter extends PageFilter {
    String cityName;
   String  title;
    Double min;
    Double max;

}
