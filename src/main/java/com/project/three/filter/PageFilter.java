package com.project.three.filter;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import static com.project.three.filter.PageSize.*;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PageFilter {

    int pageNumber=DEFAULT_PAGE;
    int pageSize=DEFAULT_PAGE_SIZE;
    String field=DEFAULT_FIELD;
    String dir = DEFAULT_SORT_DIRECTION;

}


