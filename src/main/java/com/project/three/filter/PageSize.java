package com.project.three.filter;

public interface PageSize {
    String DEFAULT_SORT_DIRECTION = "DESC";
    int DEFAULT_PAGE_SIZE = 50;
    int DEFAULT_PAGE = 0;
    String DEFAULT_FIELD="id";
}

