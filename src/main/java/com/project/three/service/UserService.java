package com.project.three.service;


import com.project.three.dto.request.jwt.JwtRequest;
import com.project.three.dto.request.user.UserLoginRequest;
import com.project.three.dto.request.user.UserRegisterRequest;
import com.project.three.dto.request.user.UserRequest;
import com.project.three.entity.basket.Basket;
import com.project.three.entity.jwt.Jwt;
import com.project.three.entity.login.Authority;
import com.project.three.entity.login.Message;
import com.project.three.entity.login.User;
import com.project.three.handler.MyExceptionMessage;
import com.project.three.mapper.UserMapper;
import com.project.three.repository.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class UserService {
    AuthRepository authRepository;
    UserRepository userRepository;
    JwtRepository jwtRepository;
    UserMapper userMapper;
    JavaMailSender javaMailSender;
    MessageRepository messageRepository;
    BasketRepository basketRepository;
   // PasswordEncoder passwordEncoder;
    String keyString = "dGhpcyBpcyB0aGFuIGV4YW1wbGV0aGlzIGlzIHRoYW4gZXhhbXBsZSB0aGlzIGlzIHRoYW4gZXhhbXBsZXRoaXMgaXMgdGhhbiBleGFtcGxl";
    final Key key= Keys.hmacShaKeyFor(Decoders.BASE64.decode(keyString));

    public void  sendMessageForRegister(UserRequest userRequest){
        Message user=userMapper.toMessage(userRequest);

        if (messageRepository.existsByEmail(user.getEmail())){
            Message message=messageRepository.findByEmail(user.getEmail());
            messageRepository.delete(message);
        }
        if (userRepository.existsByEmail(userRequest.getEmail())){
            throw new MyExceptionMessage("bu email artiq istifade olunub");
        }else {
            javaMailSender.send(send(user));
            messageRepository.save(user);
        }


        System.out.println("ending method");
    }

    public void test(Integer str){
        Message user=messageRepository.findByBody(str);
        System.out.println(user+" budu");
        if (user==null){
            throw new MyExceptionMessage("kodu duzgun daxil edin");
        }

    }

    public List<User> findAll(){
        return  userRepository.findAll();
    }



    @Transactional
    public  String register(UserRegisterRequest userRegisterRequest){
        if (userRepository.existsByEmail(userRegisterRequest.getEmail())){
            throw new MyExceptionMessage("bu mail artiq istifade olunub");
        }
        Message message=messageRepository.findByEmail(userRegisterRequest.getEmail());
        Jwt jwt=new Jwt();
        User user=userMapper.toUserRequestForUser(userRegisterRequest);

            user.setEmail(message.getEmail());
           Authority authority = authRepository.findById(1L).get();

            user.setAuthorities(Set.of(authority));
          final JwtBuilder jwtBuilder=jwtBuilder(user);
            jwt=jwtRepository.findByUser_Id(user.getId());
           // user.setBody(null);
            userRepository.save(user);
            if (jwt!=null){
                jwt.setAccessToken(jwtBuilder.compact());
            }else {
                jwt =Jwt.builder()
                        .user(user)
                        .accessToken(jwtBuilder.compact())
                        .build();

                jwtRepository.save(jwt);
                jwt.setUser(user);
            }

        messageRepository.delete(message);
            Basket basket= Basket.builder()
                    .user(user)
                    .build();
            basketRepository.save(basket);
         return jwt.getAccessToken();
    }

    public void forgetPasswordMessage(UserRequest userRequest){
        User user=userRepository.findByEmail(userRequest.getEmail());
        if (!userRepository.existsByEmail(user.getEmail())){
            throw new RuntimeException("bele bir user yoxdur");
        }
        Message message= Message.builder()
                .email(user.getEmail())
                .build();
        javaMailSender.send(send(message));
        messageRepository.save(message);

    }

    public void changeForgetPassword(UserLoginRequest userLoginRequest){
        User user=userRepository.findByEmail(userLoginRequest.getEmail());
        user.setPassword(userLoginRequest.getPassword());
        userRepository.save(user);
        Message message=messageRepository.findByEmail(userLoginRequest.getEmail());
        messageRepository.delete(message);
    }

    public String login(UserLoginRequest userLoginRequest){
        User user=userRepository.findByEmail(userLoginRequest.getEmail());
        if (user==null){
            throw new MyExceptionMessage("not found");
        } else if (!user.getPassword().equals(userLoginRequest.getPassword())){
            throw new MyExceptionMessage("not found");
        }
        return user.getJwt().getAccessToken();

    }

    public User get(Long id){
        return userRepository.findById(id).orElseThrow();
    }
    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


    private SimpleMailMessage send(Message user){
        System.out.println("sending method");
        SimpleMailMessage sm=new SimpleMailMessage();
        sm.setFrom("orxansamed111@gmail.com");
        sm.setTo(user.getEmail());
        Random body=new Random();
        user.setBody(body.nextInt(10000));
        sm.setText(String.valueOf(user.getBody()));
        return sm;
    }
    private ResponseEntity<Object> generated(String msg, HttpStatus hs, Object o){
        Map<String,Object> mp=new HashMap<>();
        mp.put("message",msg);
        mp.put("status",hs);
        mp.put("object",o);
        return new ResponseEntity<Object>(mp,hs);
    }
    private JwtBuilder jwtBuilder(User user){
        List<String> collect = user.getAuthorities()
                .stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        return Jwts.builder()
                .setSubject(user.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(
                        Duration.ofSeconds(300000))))
                .setHeader(Map.of("type", "JWT"))
                .addClaims(Map.of("role", collect))
                .signWith(key, SignatureAlgorithm.HS512);
    }
}
