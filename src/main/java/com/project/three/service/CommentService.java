package com.project.three.service;

import com.project.three.dto.request.comment.CommentRequest;
import com.project.three.entity.comment.Comment;
import com.project.three.entity.home.Home;
import com.project.three.entity.login.User;
import com.project.three.repository.CommentRepository;
import com.project.three.repository.HomeRepository;
import com.project.three.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
public class CommentService {
    HomeRepository homeRepository;
    UserRepository userRepository;
    CommentRepository commentRepository;

    public void writeComment(CommentRequest commentRequest){
        Home home=homeRepository.findById(commentRequest.getHomeId()).orElseThrow();
        User user=userRepository.findById(commentRequest.getUserId()).orElseThrow();

        Comment comment=Comment.builder()
                .comment(commentRequest.getComment())
                .home(home)
                .user(user)
                .build();

        commentRepository.save(comment);
    }
}
