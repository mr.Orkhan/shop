package com.project.three.service;

import com.project.three.dto.request.home.HomeRequest;
import com.project.three.dto.response.HomeResponse;
import com.project.three.dto.response.basket.BasketResponse;
import com.project.three.dto.response.comment.CommentResponse;
import com.project.three.dto.response.home.OneHomeResponse;
import com.project.three.entity.basket.BasketProduct;
import com.project.three.entity.comment.Comment;
import com.project.three.entity.home.Address;
import com.project.three.entity.home.Home;
import com.project.three.entity.home.HomeFirstPhoto;
import com.project.three.entity.home.HomeOtherPhoto;
import com.project.three.entity.login.User;
import com.project.three.enums.Premium;
import com.project.three.enums.Staring;
import com.project.three.filter.HomeFilter;
import com.project.three.handler.MyExceptionMessage;
import com.project.three.mapper.AddressMapper;
import com.project.three.mapper.FirstPhotoMapper;
import com.project.three.mapper.HomeMapper;
import com.project.three.repository.*;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
public class HomeService {
    HomeMapper homeMapper;
    HomeRepository homeRepository;
    AddressRepository addressRepository;
    FirstPhotoMapper firstPhotoMapper;
    FirstPhotoRepository firstPhotoRepository;
    UserRepository userRepository;
    OtherPhotoRepository otherPhotoRepository;
    AddressMapper addressMapper;
    CommentRepository commentRepository;
    BasketRepository basketRepository;
    HomeCountRepository homeCountRepository;


    @Transactional
    public void create(HomeRequest homeRequest) {
        Home home=homeMapper.forHome(homeRequest);
        if(home.getDescription()==null||home.getTitle()==null){
            throw  new MyExceptionMessage("zehmet olmasa daxil edin");
        }
        User user=userRepository.findById(homeRequest.getUserId()).orElseThrow(()-> new MyExceptionMessage("please add email"));
        home.setUser(user);
        home.setSum(0);
        home.setPersonCount(0);
        home.setPremium(Premium.NO);
        home.setLocalDateTime(LocalDateTime.now());
        homeRepository.save(home);
        Address address=addAddress(homeRequest,home);
        if (address.getAddressName()==null|| address.getCityName()==null){
            throw  new MyExceptionMessage("zehmet olmasa daxil edin");
        }
         addressRepository.save(address);
        HomeFirstPhoto homeFirstPhoto= addPhoto(homeRequest,home);
        if (homeFirstPhoto.getImage()==null){
            throw  new MyExceptionMessage("zehmet olmasa sekil daxil edin");
        }
        firstPhotoRepository.save(homeFirstPhoto);
        List<HomeOtherPhoto> photos=new ArrayList<>();
        homeRequest.getImages().forEach(tp->{
            HomeOtherPhoto hmo=HomeOtherPhoto.builder()
                    .imageString(tp)
                    .home(home)
                    .build();
            otherPhotoRepository.save(hmo);
        });
    }

    public Page<HomeResponse> findAll(HomeFilter homeFilter, Pageable pageable){
        Page<Home> homes = homeRepository.findAll((Specification<Home>) (root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.conjunction();
            if (homeFilter.getCityName() != null) {
                Join<Home,Address> joinAddress = root.join("address", JoinType.INNER);
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(joinAddress.get("cityName"),
                        "%" + homeFilter.getCityName().toLowerCase() + "%"));

            }
            if (homeFilter.getTitle() != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("title"),
                        "%" + homeFilter.getTitle().toLowerCase() + "%"));
            }
            if(homeFilter.getMin()!=null&&homeFilter.getMax()!=null){
                predicate=criteriaBuilder.and(predicate,criteriaBuilder.greaterThanOrEqualTo(root.get("price"), homeFilter.getMin()));
                predicate=criteriaBuilder.and(predicate,criteriaBuilder.lessThan(root.get("price"), homeFilter.getMax()));
            }
          return predicate;

        },pageable);


        return homes.map(homeMapper::toHomes);

    }





    @Transactional
    public OneHomeResponse get(Long id){
        Home home=homeRepository.findById(id).orElseThrow(()->new MyExceptionMessage("not found"));
        Address address=addressRepository.findByHome_id(id);
        HomeFirstPhoto homeFirstPhoto=firstPhotoRepository.findByHome_id(id);
        OneHomeResponse oneHomeResponse=homeMapper.toOneResponse(home);
        List<CommentResponse> commentResponseList=new ArrayList<>();
        List<HomeOtherPhoto> homeOtherPhotoList=otherPhotoRepository.findByHome_id(id);
         homeOtherPhotoList.forEach(a->{
            oneHomeResponse.setImages(List.of(a.getImageString()));

         });
         List<Comment> commentList=commentRepository.findByHome_id(id);
        commentList.forEach(h->{
             CommentResponse commentResponse= CommentResponse.builder()
                     .comment(h.getComment())
                     .user(h.getUser().getUsername())
                     .build();
             commentResponseList.add(commentResponse);

         });
        oneHomeResponse.setAddressName(address.getAddressName());
        oneHomeResponse.setCityName(address.getCityName());
        oneHomeResponse.setImage(homeFirstPhoto.getImage());
         oneHomeResponse.setCommentResponses(commentResponseList);
        return oneHomeResponse;
    }

    public void addPremium(Long id){
        Home home=homeRepository.findById(id).orElseThrow(()->new MyExceptionMessage("dont correct"));
        home.setPremium(Premium.PREMIUM);
        homeRepository.save(home);
    }

    @Transactional
    public void addBasket(Long id,Long userId){
        BasketProduct basketProduct =new BasketProduct();
        AtomicReference<Integer> count= new AtomicReference<>(0);
        User user=userRepository.findById(userId).orElseThrow();
        Home home=homeRepository.findById(id).orElseThrow(()->new MyExceptionMessage("not found"));
    List<BasketProduct> bp=homeCountRepository.findByBasket_id(user.getBasket().getId());
        for (BasketProduct product : bp) {
            if (product.getHome() == home) {
                product.setCount(product.getCount() + 1);
                count.getAndSet(count.get() + 1);
                homeCountRepository.save(product);
            }
        }
         if (count.get()==0){
             basketProduct=BasketProduct.builder()
                     .basket(user.getBasket())
                     .home(home)
                     .count(1)
                     .build();
             homeCountRepository.save(basketProduct);
         }

        basketRepository.save(user.getBasket());
    }

    @Transactional
    public void removeBasket(Long id,Long userId) {
        Home home = homeRepository.findById(id).orElseThrow();
        BasketProduct basketProduct = new BasketProduct();
        AtomicReference<Integer> count = new AtomicReference<>(0);
        User user = userRepository.findById(userId).orElseThrow();
        List<BasketProduct> bp = homeCountRepository.findByBasket_id(user.getBasket().getId());
        for (BasketProduct product : bp) {
            if (product.getHome() == home) {
                if (product.getHome().equals(home)) {
                    if (product.getCount() == 1) {
                        homeCountRepository.delete(product);

                    } else {
                        product.setCount(product.getCount() - 1);
                        homeCountRepository.save(product);
                    }

                }
            }
        }
    }

    @Transactional
    public List<BasketResponse> allProduct(Long id){
        User user=userRepository.findById(id).orElseThrow();
        List<BasketResponse> basketResponses=new ArrayList<>();
        List<BasketProduct> bp=homeCountRepository.findByBasket_id(user.getBasket().getId());
        for (BasketProduct product : bp) {
            BasketResponse basketResponse= BasketResponse.builder()
                    .hotelName(product.getHome().getTitle())
                    .price(product.getHome().getPrice()*product.getCount())
                    .count(product.getCount())
                    .build();
            basketResponses.add(basketResponse);
        }
        return basketResponses;
    }


    public void addStar(Staring staring,Long id){
        Home home=homeRepository.findById(id).orElseThrow(()->new MyExceptionMessage("dont correct"));
        Integer avg=avg(staring,home.getSum(),home.getPersonCount());
        switch (avg) {
            case 1 -> {
            star(home,Staring.STARING_1,1);
                homeRepository.save(home);
            }
            case 2 -> {
                star(home,Staring.STARING_2,2);
                homeRepository.save(home);
            }
            case 3 -> {
                star(home,Staring.STARING_3,3);
                homeRepository.save(home);
            }
            case 4 -> {
                star(home,Staring.STARING_4,4);
                homeRepository.save(home);
            }
            case 5 -> {
                star(home,Staring.STARING_5,5);
                homeRepository.save(home);
            }
        }
    }

    private Integer avg(Staring staring,Integer sum,Integer c){
        int number=Integer.parseInt(String.valueOf(staring.toString().charAt(8)));
        return (sum+number)/ (c+1);

    }
    private void star(Home home,Staring staring,Integer sum){
        home.setStaring(staring);
        home.setSum(home.getSum()+sum);
        home.setPersonCount(home.getPersonCount()+1);
    }
    private Address addAddress(HomeRequest homeRequest,Home home){
        Address address=addressMapper.toAddress(homeRequest);

        address.setHome(home);
        return  address;
    }
    private HomeFirstPhoto addPhoto(HomeRequest homeRequest,Home home){
        HomeFirstPhoto homeFirstPhoto=firstPhotoMapper.toPhoto(homeRequest);

        homeFirstPhoto.setHome(home);
        return homeFirstPhoto;

    }

}
