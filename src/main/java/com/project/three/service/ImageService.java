package com.project.three.service;

import com.project.three.entity.login.User;
import com.project.three.entity.photo.ImageData;
import com.project.three.entity.photo.PhotoEdit;
import com.project.three.mapper.PhotoMapper;
import com.project.three.repository.ImageRepository;
import com.project.three.repository.PhotoRepository;
import com.project.three.repository.UserRepository;
import com.project.three.util.ImageUtils;
import com.project.three.dto.request.photo.PhotoWrapper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class ImageService {
    ImageRepository imageRepository;
    UserRepository userRepository;
    PhotoRepository photoRepository;
    PhotoMapper photoMapper;

    @Transactional
    public void uploadImage(MultipartFile file,String email) throws IOException {
       User user=userRepository.findByEmail(email);
       ImageData imageData= ImageData.builder()
               .name(file.getOriginalFilename())
               .imageData(ImageUtils.compressImage(file.getBytes()))
               .build();
        System.out.println("bura kimiela...");
       imageData.setUser(user);
        imageRepository.save(imageData);

    }

    public byte[] downloadImage(String fileName){
        ImageData imageData=imageRepository.findByName(fileName);
        byte[] images=ImageUtils.decompressImage(imageData.getImageData());
        return images;
    }

  public void upload(PhotoWrapper photoWrapper,String email){
      User user = userRepository.findByEmail(email);
      PhotoEdit photoEdit=photoMapper.toEdit(photoWrapper);
      photoEdit.setUser(user);
      photoRepository.save(photoEdit);
    }



}
