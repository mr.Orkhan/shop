package com.project.three.entity.comment;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.three.entity.home.Home;
import com.project.three.entity.login.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String comment;

    @ManyToOne
            @JsonIgnore
    Home home;

    @ManyToOne
    User user;
}
