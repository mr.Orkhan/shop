package com.project.three.entity.login;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.three.entity.basket.Basket;
import com.project.three.entity.comment.Comment;
import com.project.three.entity.home.Home;
import com.project.three.entity.jwt.Jwt;
import com.project.three.entity.photo.ImageData;
import com.project.three.entity.photo.PhotoEdit;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(nullable = false)
    String name;
    @Column(nullable = false)
    String surname;
    @JsonFormat(pattern="yyyy-MM-dd")
    String birthDay;
    @Column(nullable = false)
    String username;

    String email;
    @Column(nullable = false)
    String password;


    @OneToOne(mappedBy = "user")
    @JsonIgnore
    Jwt jwt;

    @OneToOne(mappedBy = "user")
    @JsonIgnore
    PhotoEdit photoEdit;

    @OneToOne(mappedBy = "user")
    @JsonIgnore
    ImageData imageData;

    @OneToOne(mappedBy = "user")
    @JsonIgnore
    Basket basket;

//    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//            @JsonIgnore
//    List<Home> homeList;
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    List<Comment> commentList;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }







    @JsonIgnore
    @ManyToMany( cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @Builder.Default
    Set<Authority> authorities=new HashSet<>();

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

