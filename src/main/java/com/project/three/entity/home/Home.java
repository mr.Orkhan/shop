package com.project.three.entity.home;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.three.entity.basket.BasketProduct;
import com.project.three.entity.comment.Comment;
import com.project.three.entity.login.User;
import com.project.three.enums.Premium;
import com.project.three.enums.Staring;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Home {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false)
    String title;
    @Column(nullable = false)
    String description;
    @Column(nullable = false)
    Double price;

    @DateTimeFormat
    LocalDateTime localDateTime;

    @Enumerated(EnumType.STRING)
    Staring staring;

    Integer personCount;
    Integer sum;
    @Enumerated(EnumType.STRING)
    Premium premium;
    @ManyToOne
    //@JsonIgnore
    User user;

}
