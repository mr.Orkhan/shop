package com.project.three.entity.home;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.three.entity.login.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class HomeOtherPhoto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(length = 100000)
    String imageString;

    @ManyToOne
    @JsonIgnore
    Home home;

}
