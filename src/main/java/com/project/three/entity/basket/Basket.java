package com.project.three.entity.basket;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.three.entity.home.Home;
import com.project.three.entity.login.Authority;
import com.project.three.entity.login.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
   // Integer count;
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    User user;

    @OneToMany(mappedBy = "home",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    List<BasketProduct> basketProducts;
}
