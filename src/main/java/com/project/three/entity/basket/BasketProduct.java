package com.project.three.entity.basket;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.three.entity.home.Home;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BasketProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Integer count;

    @ManyToOne
    @JsonIgnore
    Home home;

    @ManyToOne
    @JsonIgnore
    Basket basket;
}
