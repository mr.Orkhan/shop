package com.project.three.controller;

import com.project.three.dto.request.comment.CommentRequest;
import com.project.three.dto.request.home.HomeFirstPhotoRequest;
import com.project.three.dto.request.home.HomeRequest;
import com.project.three.dto.request.home.TestPhotosRequest;
import com.project.three.dto.response.HomeResponse;
import com.project.three.dto.response.basket.BasketResponse;
import com.project.three.dto.response.home.OneHomeResponse;
import com.project.three.entity.home.Home;
import com.project.three.enums.Staring;
import com.project.three.filter.HomeFilter;
import com.project.three.service.CommentService;
import com.project.three.service.HomeService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/home")
public class HomeController {
    private final HomeService homeService;
    private final CommentService commentService;


    @PostMapping("/add-elan")
    public void addElan(@RequestBody  HomeRequest request) throws Exception {
        homeService.create(request);
    }

    @PutMapping("/valuation")
    public void addStar(@RequestParam("staring") Staring staring,
                        @RequestParam("id") Long id){
        homeService.addStar(staring, id);
    }

    @PostMapping("/comment-for-home")
    public void writeComment(@RequestBody CommentRequest commentRequest){
        commentService.writeComment(commentRequest);

    }

    @GetMapping("/any")
    public OneHomeResponse get(Long id){
       return homeService.get(id);
    }


    @GetMapping("/find-all")
   public Page<HomeResponse> getAll(HomeFilter filter) {
        return homeService.findAll(filter, PageRequest.of(filter.getPageNumber(),filter.getPageSize()
                , Sort.by(Sort.Direction.fromString(filter.getDir()), filter.getField())));
    }

    @PostMapping("/add-basket")
    public void addBasket(Long id,Long userId){
       homeService.addBasket(id, userId);
    }

    @DeleteMapping("/remove-home-for-basket")
    public void removeProduct(Long id,Long userId){
        homeService.removeBasket(id, userId);
    }

    @GetMapping("/basket-products")
    public List<BasketResponse> allProduct(Long id){
return homeService.allProduct(id);
    }
}
