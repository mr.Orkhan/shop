package com.project.three.controller;


import com.project.three.dto.request.photo.PhotoWrapper;
import com.project.three.dto.request.user.UserLoginRequest;
import com.project.three.dto.request.user.UserRegisterRequest;
import com.project.three.dto.request.user.UserRequest;
import com.project.three.entity.login.User;
import com.project.three.service.ImageService;
import com.project.three.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v2/user")
public class UserController {
    private final UserService userService;
    private final ImageService imageService;


    @PostMapping("/send-email-code")
    public void  send(@RequestBody UserRequest mail){
        userService.sendMessageForRegister(mail);
    }


    @PutMapping("/approve-code")
    public void  test(@RequestParam("code") Integer code){
       userService.test(code);
    }

    @PostMapping("/register")
    public String  register(@RequestBody UserRegisterRequest userRegisterRequest){
       return userService.register(userRegisterRequest);
    }

    @PostMapping("/send-message-for-forget-password")
    public void  forgetPasswordMessage(@RequestBody UserRequest mail){
        userService.forgetPasswordMessage(mail);
    }

    @PutMapping("/forget-password")
    public void  changeForgetPassword(@RequestBody UserLoginRequest userLoginRequest){
        userService.changeForgetPassword(userLoginRequest);
    }

    @PostMapping("/login")
    public String  login(@RequestBody UserLoginRequest userLoginRequest){//@PathVariable String email, @PathVariable String password){
        return userService.login(userLoginRequest);
    }

    @GetMapping("/find-all")
    public List<User> findAll(){
        return  userService.findAll();
    }

    @GetMapping("/get/{id}")
    public User get(@PathVariable Long id){
        return  userService.get(id);
    }

//    @PostMapping("/add-photo")
//    public ResponseEntity<?> addPhotos(@RequestParam("image")MultipartFile file,
//                                       @RequestParam("email")String email) throws IOException {
//        imageService.uploadImage(file,email);
//        return ResponseEntity.status(HttpStatus.OK)
//                .body("tebrikler");
//    }
//    @GetMapping("/show-photo/{fileName}")
//    public ResponseEntity<?> addPhotos(@PathVariable String fileName) throws IOException {
//        return ResponseEntity.status(HttpStatus.OK)
//                .contentType(MediaType.valueOf("image/png"))
//                .body(  imageService.downloadImage(fileName));
//    }
//
//    @PostMapping("/add-photo")
//    public void upload(@RequestBody PhotoWrapper photoWrapper,
//                       @RequestParam("email")String email){
//        imageService.upload(photoWrapper, email);
//    }
}
