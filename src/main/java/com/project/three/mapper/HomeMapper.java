package com.project.three.mapper;


import com.project.three.dto.request.home.HomeRequest;
import com.project.three.dto.response.HomeResponse;
import com.project.three.dto.response.home.OneHomeResponse;
import com.project.three.entity.home.Home;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface HomeMapper {



    List<HomeResponse> toList(List<Home> byPriceBetween);


//    @Mapping(expression ="java(address.address)",target = "address")
//    @Mapping( expression ="java(address.cityName)",target = "cityName")
//    @Mapping( expression ="java(homeFirstPhoto.imageString)",target = "image")
//    @Mapping(expression ="java(user.id)",target = "userId")
//@Mapping(source = "address.address", target = "address")
//@Mapping(source = "address.cityName", target = "cityName")
//@Mapping(source = "homeFirstPhoto.imageString", target = "image")
//@Mapping(source = "user.id", target = "userId")
  Home forHome(HomeRequest homeRequest);
//    @Mapping(target = "cityName",source = "address.cityName")
//    @Mapping(target = "image",source = "homeFirstPhoto.image")
    HomeResponse toHomes(Home home);

//
//    @Mapping(target = "cityName",source = "address.cityName")
//    @Mapping(target = "addressName",source = "address.addressName")
//    @Mapping(target = "image",source = "homeFirstPhoto.image")
    OneHomeResponse toOneResponse(Home home);
}
