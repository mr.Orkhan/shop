package com.project.three.mapper;

import com.project.three.dto.request.home.HomeRequest;
import com.project.three.entity.home.HomeFirstPhoto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface FirstPhotoMapper {

    HomeFirstPhoto toPhoto(HomeRequest homeRequest);
}
