package com.project.three.mapper;

import com.project.three.dto.request.photo.PhotoWrapper;
import com.project.three.entity.photo.PhotoEdit;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface PhotoMapper {

    PhotoEdit toEdit(PhotoWrapper photoWrapper);
}
