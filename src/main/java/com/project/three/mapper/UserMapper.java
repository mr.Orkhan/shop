package com.project.three.mapper;

import com.project.three.dto.request.user.UserRegisterRequest;
import com.project.three.dto.request.user.UserRequest;
import com.project.three.entity.login.Message;
import com.project.three.entity.login.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UserMapper {
    User toUser(UserRequest userRequest);

    User toRegister(UserRegisterRequest userRegisterRequest);

    Message toMessage(UserRequest userRequest);

    User toUserRequestForUser(UserRegisterRequest userRegisterRequest);
}

